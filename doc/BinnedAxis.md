# Continuous and discrete axes

YODA's axis class is templated on the edge type,
giving rise to different semantics.

## Continuous axes

The most common axis is the continuous axis.

```cpp
const auto caxis = Axis<double>{{0.1, 2.3, 4.5}};
```
The continuous axis is like a number axis spanning
the full range from `-inf` to `+inf`.

The `N` edges from the constructor therefore divides
the axis into `N+1` bins. That is `N-1` visible bins,
one underflow going from `-inf` to the lowest edge
and one overflow going from the highest edge to `+inf`.

The `edges()` method will return all edges, including
`-inf` and `+inf`:

```cpp
std::cout << "Continuous axis with " << caxis.edges().size() << " floating-point edges:" << std::endl;
for (auto edge : caxis.edges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
```

```
Continuous axis with 5 floating-point edges:
	-inf	0.1	2.3	4.5	inf
```
The `size()` method returns the number of bins,
including under-/overflows:
```cpp
std::cout << "This axis has " << caxis.numBins(true) << " bins in total." << std::endl;
```

```
This axis has 4 bins in total.
```


## Discrete axes

If the edge type is not a floating point type,
the axis will be discrete.

```cpp
const auto daxis1 = Axis<int>{{-3, -2, -1, 0, 1, 2, 3}};
const auto daxis2 = Axis<std::string>{{"A", "B", "C"}};
```
The discrete axis is like a collection of "labelled categories",
with an additional "otherflow" for anything that doesn't match
the a label specified in the constructor.
The `N` edges from the constructor therefore correspond to
`N+1` bins.

The `edges()` method will return all edges that have a label:

```cpp
std::cout << "Discrete axis with " << daxis1.edges().size() << " integral edges:" << std::endl;
for (auto edge : daxis1.edges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;

std::cout << "Discrete axis with " << daxis2.edges().size() << " string edges:" << std::endl;
for (auto edge : daxis2.edges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
```

```
Discrete axis with 7 integral edges:
	-3	-2	-1	0	1	2	3
Discrete axis with 3 string edges:
	A	B	C
```
The `numBins()` method returns the number of bins,
excluding under-/overflows. An optional Boolean
is accepted to include them:
```cpp
std::cout << "This axis has " << daxis1.numBins(true) << " bins in total." << std::endl;
std::cout << "This axis has " << daxis2.numBins(true) << " bins in total." << std::endl;
```

```
This axis has 8 bins in total.
This axis has 4 bins in total.
```
