# Estimates

Unlike the [Dbn class](Dbn.md) which is a live
object that updates its internal state upon
a fill, YODA also provides a dedicated inert type
that distributions turn into once they are "finished",
i.e. no longer fillable: the `Estimate` class.

This is essentially a central value with
an associated uncertainty breakdown which lends
itself nicely to measured quantities.

```cpp
Estimate e1;
e1.setVal(42); // set central value
e1.setErr({-4,5}); // set (down, up) error pair
std::cout << e1._toString() << std::endl;
std::cout << "relError()=" << e1.relErr().first;
std::cout << ", " << e1.relErr().second << std::endl;
```

```
value=42.000000, user-supplied total error()={ -4.000000, 5.000000 }
relError()=-0.0952381, 0.119048
```

**Note that error pairs without a label will be interpreted
as the total uncertainty.**


## Error pairs and labelling conventions

Please mind the following conventions:
* An error pair is interpreted as the resulting uncertainty
  due to a (downward,upward) shift in the corresponding
  nuisance parameter. YODA stores these internally as
  (down,up) components:
  ```cpp
  const std::string typeA("uncor,typeA");
  Estimate e2(2, {-2,3}, typeA);
  std::cout << "err()=(" << e2.err(typeA).first << ",";
  std::cout << e2.err(typeA).second << ")" << std::endl;
  std::cout << "(down,up)=(" << e2.errDown(typeA);
  std::cout << "," << e2.errUp(typeA) << ")" << std::endl;
  ```
  ```
  err()=(-2,3)
  (down,up)=(-2,3)
  ```
* The negative sign for an error pair could be on either the down or the up component
  depending on the effect of the systematic shift in the nuisance parameter:
  ```cpp
  const std::string typeC("uncor,typeC");
  e2.setErr({1,-1}, typeC);
  std::cout << "(down,up)=(" << e2.errDown(typeC) << ",";
  std::cout << e2.errUp(typeC) << ")" << std::endl;
  std::cout << "(neg,pos)=(" << e2.errNeg(typeC) << ",";
  std::cout << e2.errPos(typeC) << ")" << std::endl;
  ```
  ```
  (down,up)=(1,-1)
  (neg,pos)=(-1,1)
  ```
* If both components have the same sign, the error pair will be treated as one sided.
  When asking for signed components, the envelope will be used for one-sided pairs.
  ```cpp
  const std::string typeD("uncor,typeD");
  e2.setErr({0.5,0.8}, typeD);
  std::cout << "(down,up)=(" << e2.errDown(typeD) << ",";
  std::cout << e2.errUp(typeD) << ")" << std::endl;
  std::cout << "(neg,pos)=(" << e2.errNeg(typeD) << ",";
  std::cout << e2.errPos(typeD) << ")" << std::endl;
  ```
  ```
  (down,up)=(0.5,0.8)
  (neg,pos)=(0,0.8)
  ```
* The sum of quadrature will combine all negatively and all positive error components and
  returns the (negative,positive) quadrature sum pair.
  ```cpp
  e2.setErr({-2,2}, "cor,typeB");
  std::cout << "quadSum()=(" << e2.quadSum().first << ", ";
  std::cout << e2.quadSum().second << ")" << std::endl;
  ```
  ```
  quadSum()=(-3, 3.82623)
  ```
  where `neg = quadSum(-2,-1,0,-2) = -3` and `pos =  quadSum(3,1,0.8,2) = 3.82623`.
* In the absence of a user-supplied total, the total uncertainty will be calculated
  from the sum in quadrature:
  ```cpp
  std::cout << "totalErr()=(" << e2.totalErrNeg() << ", ";
  std::cout << e2.totalErrPos() << ")" << std::endl;
  ```
  ```
  totalErr()=(-3, 3.82623)
  ```
* The empty string is interpreted as a user-supplied total uncertainty,
  that may very well be different from the sum in quadrature of the individual error components.
  A user-supplied total will always take precendece.
  ```cpp
  e2.setErr({-4,5});
  std::cout << "totalErr()=(" << e2.totalErrNeg() << ", ";
  std::cout << e2.totalErrPos() << ")" << std::endl;
  ```
  ```
  totalErr()=(-4, 5)
  ```
* The error label should not be equal to the string `"total"` (irrespective of case).
* In arithmetic operations between different AOs, uncertainty
  components that have the same label will be treated as
  correlated between AOs, unless the label starts with `stat`
  or `uncor`, in which case it will be treated as uncorrelated
  between different AOs. This behvaviour can be customised by
  passing a suitable regular expression to the arithmetic methods
  (e.g. `add`, `subtract`, `divide`) which matches the uncorrelated
  uncertainty components.
* Uncorrelated uncertainty components combine in quadrature:
  ```cpp
  Estimate e3(2, {-4,3}, typeA);
  Estimate e4(1, {-3,4}, typeA);
  std::cout << (e3+e4)._toString() << std::endl;
  std::cout << (e3-e4)._toString() << std::endl;
  ```
  ```
  value=3.000000, error(uncor,typeA)={ -5.000000, 5.000000 }
  value=1.000000, error(uncor,typeA)={ -5.000000, 5.000000 }
  ```
* Correlated uncertainty components
  combine linearly:
  ```cpp
  const std::string typeB("cor,typeB");
  Estimate e5(2, {-4,3}, typeB);
  Estimate e6(1, {-3,4}, typeB);
  std::cout << (e5+e6)._toString() << std::endl;
  std::cout << (e5-e6)._toString() << std::endl;
  ```
  ```
  value=3.000000, error(typeA)={ -7.000000, 7.000000 }
  value=1.000000, error(typeA)={ -7.000000, 7.000000 }
  ```
* When dividing Estimates, the relative uncertainty components are added
  in quadrature in case the components are uncorrelated, the resulting
  uncertainty component is evaluated based on the correlated ratios in
  case the component is correlated:
  ```cpp
  std::cout << (e3/e4)._toString() << std::endl; // (dR/R) ** 2 = (dN/N) ** + (dD/D) **2
  std::cout << (e5/e6)._toString() << std::endl; // (R+dR) = (N+dN)/(D+dD)
  ```
  ```
  value=2.000000, uncorrelated error(uncor,typeA)={ -7.211103, 8.544004 }
  value=2.000000, correlated error(cor,typeB)={ -1.000000, -1.000000 }
  ```
