# YODA ASCII layout

The YODA ASCII format writes out analysis objects (AOs) in blocks.
Each block begins with a line that contains the `BEGIN` key word;
a string that is constructed from `YODA`, the type of the AO
and the current version of the ASCII YODA format, all seperated by
underscores; and the `path` field of the AO. The block ends with a
line that contains the `END` key word as well as the same versioned
type string from before, e.g. like so in case of an `Estimate1D`:

```
BEGIN YODA_ESTIMATE1D_V3 /MY_ANALYSIS/histo
...
END YODA_ESTIMATE1D_V3
```

The content of each block depends on the type of `AnalysisObject`.
First comes the metadata: The first few lines will contain the
`Path`, `Type` and `Title` field from the `AnalysisObject`,
followed by any other annotations that the AO might have had.
The annotations system supports YAML. Most of the time,
this will look something like this:

```
Path: /MY_ANALYSIS/histo
Title: ~
Type: Estimate1D
```

The metadata is separated from the numerical data by a single line
of three dashes

```
---
```

Depending on the object, this will be followed by a few object-specific status lines:
  *  `BinnedHisto`-like objects will tyically include a few global quantities about the
     histogram such as mean and integral. These would be preceded by a `#` character, like so
     ```
     # Mean: 2.095387e-03
     # Integral: 3.000000e+00
     ```
  *  Binned objects will includes a line of the bin edges for each axis, e.g. in case of a
     binned object with one axis that has two bins between 0 and 1, this would look as follows:
     ```
     Edges(A1): [0.000000e+00, 5.000000e-01, 1.000000e+00]
     ```
  *  Estimates will also include aline with the error source labels, which can be large
     array for HepData-based reference data, but often this could just look something
     like this
     ```
     ErrorLabels: ["stats"]
     ```
Immediately before the numerical data, there will be a single status line preceded by a `#` character
which includes a short title for each column that is about to follow:
  * In case of an estimate, this could like this:
    ```
    # value       errDn(1)      errUp(1)
    ```
    which indicates that the first column corresponds to the estimate value, while the second and third
    column will correspond to the down- and up-component relating to the first error source label.
  * In case of histograms, this would typically look something like this
    ```
    # sumW        sumW2         sumW(A1)      sumW2(A1)     numEntries
    ```
    which indicate that the first two columns correspond to the sum of weights and sum of squared weights,
    followed by the first and second moment along each axis versions, and finally a column for the
    raw number of events.
 *  For scatters the columns will contain value, negative and positive error colums - a triplet per dimension,
    e.g. in 1D like so:
    ```
    # val1        err1-         err1+
    ```

This is then followed by columns of the numerical values where each row corresponds to the bin/point
(including overflows where applicable).



## Differences between YODA1 and YODA2

As part of YODA2, the analysis objects were generalised to arbitrary dimensions,
which resulted in a version 3 of the YODA ASCII format following a few necessary
changes to the layout. What's changed?

The bin edges for binned objects (which used to have their own columns
in the numerical data block) had to be taken out as the amount of duplication
for higher dimensional objects would have otherwise gotten out of hand.

The error breakdown that used to be part of the metadata annotated on [Scatter](Scatter.md)
objects in in YODA1 has now been removed entirely from the scatters. Instead a new object type,
the [Estimate](Estimate.md), has been introduced which includes intrisic support
for values with an associated uncertainty breakdown.

