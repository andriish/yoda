# Unbinned estimates

YODA's `Estimate0D` class is an unbinned version of the
[BinnedEstimate containers](BinnedEstimate.md). It is the
type reduction of the [Counter class](Counter.md) and lends
itself nicely to represent integrated cross-section measurements.

It behaves just like the [Estimate](Estimate.md), but also
derives from [AnalysisObject](AnalysisObject.md).

