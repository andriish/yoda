import yoda
import numpy as np


def safeDiv(numer, denom):
    """Simple division method with implicit divByZero protection."""
    return np.divide(numer, denom,
                     out = np.where(numer != 0, np.nan, 1),
                     where = denom != 0)


def reshape(xprobe, xref, yvals, xerrs = None, yerrs = None):
    """Helper method to reshape yvals and yerrs to match shape of reference curve.
       If the probe has fewer elements than the reference, pad using NaNs."""
    padded_yvals = np.array([ yvals[x==xprobe][0] if x in xprobe else \
                              np.nan for x in xref ])
    padded_xerrs = None if xerrs is None else \
                   np.array([ [errs[x==xprobe][0] if x in xprobe else \
                               np.nan for x in xref ] for errs in xerrs ])
    padded_yerrs = None if yerrs is None else \
                   np.array([ [errs[x==xprobe][0] if x in xprobe else \
                               np.nan for x in xref ] for errs in yerrs ])
    return padded_yvals, padded_xerrs, padded_yerrs


def mkPlotFriendlyScatter(ao):
  """Converts non-scatter AOs to scatters. If the AO has masked bins,
     the corresponding indices are stored in the metadata.If the AO has
     discrete binning, the edge labels are also stored in the metadata."""
  origT = str(type(ao))
  rtn = ao.mkScatter(ao.path()) if 'Binned' not in origT else \
        ao.mkScatter(ao.path(), includeOverflows=False,
                     includeMaskedBins=True) if 'Estimate' in origT else \
        ao.mkScatter(ao.path(), binwidthdiv=True, useFocus=False,
                     includeOverflows=False,includeMaskedBins=True)
  if hasattr(ao, 'binDim'):
      # if there are masked bins, find them and set value to NaN
      if len(ao.maskedBins()) > 0:
          maskIdx = -1
          for b in ao.bins(False, True):
              maskIdx += 1
              if b.isMasked():
                  rtn.point(maskIdx).setVal(ao.dim()-1, np.nan)

      # if there are discrete axes, set a +/-0.5 dummy uncertainty
      # and set custom tick-mark labels
      for i, axis in enumerate(ao.axisConfig().split(',')):
          if axis != 'd':
              # add dummy uncertainty
              for p in rtn.points():
                  p.setErr(i, 0.5)
              # decorate with custom labels
              if i < 3 and rtn.hasAnnotation('EdgesA%d' % (i+1)):
                  ALPHA = 'XYZ'
                  labels = rtn.annotation('EdgesA%d' % (i+1))
                  anno = '\t'.join([ '%d\t%s' % x for x in list(zip(rtn.vals(i),labels)) ])
                  rtn.setAnnotation(ALPHA[i]+'CustomMajorTicks', anno)
  return rtn


def linear_rebin(arr, rebin):
    """Helper function to rebin input array by integer factor rebin.
       Bin values are added linearly.
    """
    newlen = len(arr) // rebin
    shape = (newlen,rebin,2) if len(arr.shape) > 1 else (newlen,rebin)
    rtn = np.sum(np.resize(arr,shape), axis=1)
    res = len(arr) % rebin # check residuals if binning uneven
    if res:  rtn[-1] += np.sum(arr[-res:],axis=0)
    return rtn


def quad_rebin(arr, rebin):
    """Helper function to rebin input array by integer factor rebin.
       Bin values are added in quadrature.
    """
    return  np.sqrt(linear_rebin(arr ** 2, rebin))


def scatter_rebin(ao, rebin):
    """Helper function to attempt an on-the-fly rebinning
       of the input Scatter2D ao by integer factor rebin.
       This operation might not be well defined if the
       points are overlapping or non-adjacent.
    """
    if rebin < 2 or ao.dim() != 2:
        return ao

    oldmins = ao.xMins()
    oldmax = ao.xMax()
    oldw = np.append(oldmins[1:], [oldmax]) - oldmins
    old2w = np.array([oldw,oldw]).transpose()
    # determine new x-values
    xmin = np.resize(oldmins,(len(oldmins)//rebin,rebin))[:,0]
    xmax = np.append(xmin[1:], [oldmax])
    xerrs = 0.5*(xmax - xmin)
    xvals = xmin + xerrs
    # determine new y-values (assume bin width division)
    neww = linear_rebin(oldw, rebin)
    new2w = np.array([neww,neww]).transpose()
    yvals = linear_rebin(oldw*ao.yVals(), rebin) / neww
    yerrs = quad_rebin(old2w*ao.yErrs(), rebin) / new2w
    # create new Scatter2D
    rtn = yoda.Scatter2D()
    for k in ao.annotations():
        rtn.setAnnotation(k, ao.annotation(k))
    rtn.addPoints(zip(xvals, yvals, xerrs, xerrs, *yerrs.transpose()))
    return rtn

